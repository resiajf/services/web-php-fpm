# php-fpm for web server

The repository creates an `php-fpm` image, based on Debian 9 (aka stretch) with
PHP 7.4 (the latest always). Some extra extensions are also installed.

The FPM engine listens to `0.0.0.0:9000` by default.

## Extensions

The extensions installed are:

 - gd _Image manipulation_
 - mysqli _Mysql DB client for PHP_
 - opcache _Improves PHP by storing in a memory cache OPCodes of PHP_

To add new extensions, follow the section ["How to install more PHP extensions"][1]
of the link.

## Volumes

It does not have any volumes specified. To work, you should mount a folder
elsewhere in the container and set `SCRIPT_FILENAME` in the FastCGI params with
the absolute route to the file.

It is recommended to use `/var/www/html`.

 [1]: https://store.docker.com/images/php